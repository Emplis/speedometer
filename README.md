# [Speedometer](https://speed.theobattrel.fr)

Show your current speed using the [Geolocation API](https://developer.mozilla.org/en-US/docs/Web/API/Geolocation_API).