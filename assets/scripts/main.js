function write_on_speedometer(value, units='') {
    const speedometer_value = document.getElementsByClassName('speedometer-value')[0];
    const speedometer_units = document.getElementsByClassName('speedometer-units')[0]; 

    speedometer_value.textContent = value;
    speedometer_units.textContent = units;
}

function update_speedometer() {
    const units = display_options.units;

    const if_null = (value) => {
        if (value == null)
            return '-';
        else
            return value;
    };

    switch (units) {
        case available_units[0]:
            write_on_speedometer(if_null(geolocation_datas.speed_kh), available_units[0]);
            break;
        case available_units[1]:
            write_on_speedometer(if_null(geolocation_datas.speed_ms), available_units[1]);
            break;
        case available_units[2]:
            write_on_speedometer(if_null(geolocation_datas.altitude_m), available_units[2]);
            break;
    }
}

function success(position) {
    const speed_ms = position.coords.speed; // velocity in m/s
    const altitude = position.coords.altitude; // heigh in meter above ellipsoid, see https://w3c.github.io/geolocation-api/#dom-geolocationcoordinates-altitude

    if (debug) {
        console.log(`speed_ms ${speed_ms}`);
        console.log(`altitude_m ${altitude}`);
    }

    if (speed_ms != null) {
        const speed_kh = speed_ms * 3.6;

        if (debug) console.log(`speed_kh ${speed_kh}`);

        if ((!isNaN(speed_ms) && isFinite(speed_ms)) && (!isNaN(speed_kh) && isFinite(speed_kh))) {
            geolocation_datas.speed_ms = Math.round(speed_ms);
            geolocation_datas.speed_kh = Math.round(speed_kh);
        }
    }

    if (altitude != null) {
        if (!isNaN(altitude) && isFinite(altitude)) {
            geolocation_datas.altitude_m = Math.round(altitude);
        }
    }

    update_speedometer();
}

function error(error) {
    request_animation_stop = true;
    
    let error_message;
    switch (error.code) {
        case -1:
            error_message = 'Unsupported browser.';
            break;
        case 1:
            error_message = 'Permission denied.';
            break;
        case 2:
            error_message = 'Position unavailable.';
            break;
        case 3:
            error_message = 'Timeout.';
            break;
        case 0:
        default:
            error_message = 'Unknown error.';
            break;
    }

    console.error(`Error code ${error.code}: ${error_message}`);
    write_on_speedometer('-', error_message);
}

function init_animation() {
    const speedometer_bg = document.getElementsByClassName('speedometer-bg')[0];

    speedometer_bg.addEventListener('animationiteration', () => {
        if (request_animation_stop)
            speedometer_bg.classList.remove('spin-animation');
    });

    speedometer_bg.classList.add('spin-animation');
}

//==========================================================//

const debug = true;

let request_animation_stop = false;

const available_units = ['km/h', 'm/s', 'm'];

let geolocation_datas = {
    speed_ms: null,
    speed_kh: null,
    altitude_m: null
};

let display_options = {
    units: 'km/h'
}

window.addEventListener('load', (event) => {
    if ('geolocation' in navigator) {
        init_animation();

        const options = {
            enableHighAccuracy: true,
            maximumAge: 0, // 0s
            timeout: 10_000 // 10s
        };

        const watchID = navigator.geolocation.watchPosition(success, error, options);
        
        document.getElementsByClassName('speedometer-fg')[0].addEventListener('click', (event) => {
            const cur_units_idx = available_units.indexOf(display_options.units);
            const new_units_idx = (cur_units_idx == available_units.length - 1) ? 0 : cur_units_idx+1;

            display_options.units = available_units[new_units_idx];

            update_speedometer();
        });
    }
    else {
        error({code: -1});
    }
});